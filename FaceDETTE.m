%Face Detection
clear all
clc
%Detect objects using Viola-Jones Algorithm
%(When the number of faces is too large (>25) the photo becomes compact with faces, so the algorithm is not
%very effective in those cases)
%To detect Face
FDetect = vision.CascadeObjectDetector;

%Read the input image
I = imread('g10.jpg');
c=0.0;
%Returns Bounding Box values based on number of objects
BB = step(FDetect,I);

figure,
imshow(I); hold on
for i = 1:size(BB,1)
    rectangle('Position',BB(i,:),'LineWidth',2,'LineStyle','-','EdgeColor','r');
    c=c+1;
end
title(['Counted Faces = ', num2str(c)]);
display(c)
hold off;